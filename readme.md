readme.md - README for GambasSomeModelLib v0.4


Purpose:
Library for implementation of a business object model example called SomeModel, with a base class called ModelBase.

Requires GambasAppUtilityLib library 0.4 or later.

Note: Gambas3 projects ('.project') are looking for libraries ('<filename>.gambas') in /home/<user_name>/.local/share/gambas3/lib/<vendor_prefix>/.Make (Project|Make|Executable...) writes the executable there in addition to the root of the project's directory.


Usage notes:

~...


Setup:

~...


0.4: 
~add license file
~add readme
0.3: 
~internationalization prep, remove extraneous Settings.Save
0.2: 
~handle id not present in settings on open; fix spaces saved at end of key names in settings; handle new key id not triggering Key PropertyChanged notification for client view.
0.1: 
~initial release with SomeModel and ModelBase classes imported.

Fixes:

Known Issues:
~

Possible Enhancements:


Steve Sepan
ssepanus@yahoo.com
2/14/2022